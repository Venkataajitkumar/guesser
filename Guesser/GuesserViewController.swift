//
//  GuesserViewController.swift
//  Guesser
//
//  Created by student on 2/27/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class GuesserViewController: UIViewController {
    
    @IBOutlet weak var resultLBL: UILabel!
    @IBOutlet weak var guessTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        Guesser.shared.createNewProblem()
    }
    
    @IBAction func AmIRightBTN(_ sender: Any) {
        if let guess = Int(guessTF.text!){
                if guess > 0 && guess < 11{
                let result = Guesser.shared.AmIRight(guess: Int(guessTF.text!) ?? 0)
                if result.rawValue == "Correct"{
                    displayMessage()
                    Guesser.shared.createNewProblem()
                    resultLBL.text? = "Correct"
                }
                else if result.rawValue == "Too Low"{
                    resultLBL.text? = "Too Low"
                }
                else{
                    resultLBL.text? = "Too High"
                }
            }
                else{
                    let alertController = UIAlertController(title: "", message: "Enter number between 1 ... 10", preferredStyle: .alert) 
                    let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alertController.addAction(action)
                    
                    self.show(alertController, sender: nil)
            }
        }
        else{
            let alertController = UIAlertController(title: "", message: "Invalid data", preferredStyle: .alert)  
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(action)
            
            self.show(alertController, sender: nil)
        }
    }
    
    @IBAction func CreateNewProblemBTN(_ sender: Any) {
        Guesser.shared.createNewProblem()
    }
    func displayMessage(){
        let alert = UIAlertController(title: "Well done",
                                      message: "You got it in \(Guesser.numAttenpts) tries",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


