//
//  GuesserStatisticsViewController.swift
//  Guesser
//
//  Created by student on 2/27/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class GuessStatisticsViewController: UIViewController {
    
    @IBOutlet weak var meanLbl: UILabel!
    @IBOutlet weak var stdDevLBL: UILabel!
    @IBOutlet weak var maxLBL: UILabel!
    @IBOutlet weak var milLBL: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func ClearStatsBTN(_ sender: Any) {
        milLBL.text = "0"
        maxLBL.text = "0"
        meanLbl.text = "0"
        stdDevLBL.text = "0"
        Guesser.shared.clearStatistics()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        values()
    }
    func values(){
        milLBL.text = String(Guesser.shared.minimumNumAttempts())
        maxLBL.text = String(Guesser.shared.maximumNumAttempts())
        meanLbl.text = String(Guesser.shared.mean())
        stdDevLBL.text = String(Guesser.shared.stdDvn())
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}
