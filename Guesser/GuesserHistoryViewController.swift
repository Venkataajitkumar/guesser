//
//  GuesserHistoryViewController.swift
//  Guesser
//
//  Created by student on 2/27/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class GuessHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var historyView: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Guesser.shared.numGuesses()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "guesses")
        cell?.textLabel?.text = "Correct Answer: \(String(Guesser.shared[indexPath.row].correctAnswer))"
        cell?.detailTextLabel?.text = "#attempts: \(String(Guesser.shared[indexPath.row].numAttemptsRequired))"
        return cell!
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        historyView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        historyView.dataSource = self
        historyView.delegate = self
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
}



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


