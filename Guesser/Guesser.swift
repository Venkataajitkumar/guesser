//
//  Guesser.swift
//  Guesser
//
//  Created by student on 2/27/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation

class Guesser{
    private init(){}
    static let shared = Guesser()
    private var correctAnswer:Int = 0
    private var _numAttempts = 0
    private var guesses:[Guess] = []
    
    static var numAttenpts:Int{
        get{
            return shared._numAttempts
        }
    }
    
    func AmIRight(guess:Int) -> Result{
        if guess == correctAnswer {
            //            guesses.append(Guesser.Guess.init(correctAnswer: correctAnswer, numAttemptsRequired: _numAttempts))
            add(Guess.init(correctAnswer: correctAnswer, numAttemptsRequired: _numAttempts))
            print(guesses)
            return Result.correct
        }
        else if guess < correctAnswer {
            _numAttempts = _numAttempts+1
            return Result.tooLow
        }
        _numAttempts = _numAttempts+1
        return Result.tooHigh
        
    }
    func createNewProblem(){
        self.correctAnswer = Int.random(in: 1...10)
        _numAttempts = 1
    }
    func guess(index:Int) -> Guess{
        return guesses[index]
    }
    subscript(guess:Int) -> Guess{
        return guesses[guess]
    }
    func numGuesses() -> Int{
        return guesses.count
    }
    func clearStatistics(){
        guesses.removeAll()
    }
    private func add(_ guess:Guess){
        guesses.append(guess)
    }
    func minimumNumAttempts() -> Int{
        if guesses.isEmpty{
            return 0
        }
        var min = guesses[0].numAttemptsRequired
        for i in guesses{
            if min > i.numAttemptsRequired{
                min = i.numAttemptsRequired
            }
        }
        return min
    }
    func maximumNumAttempts() -> Int{
        if guesses.isEmpty{
            return 0
        }
        var max = guesses[0].numAttemptsRequired
        for i in guesses{
            if max < i.numAttemptsRequired{
                max = i.numAttemptsRequired
            }
        }
        return max
    }
    func mean() -> Int{
        if numGuesses() == 0{
            return 0
        }
        var numValue = 0
        for i in guesses{
            numValue += i.numAttemptsRequired
        }
        return numValue/numGuesses()
    }
    func stdDvn() -> Int{
        if numGuesses() == 0{
            return 0
        }
        var sum = 0
        for i in guesses{
            sum += (mean() - i.numAttemptsRequired) * (mean() - i.numAttemptsRequired)
        }
        return Int(sqrt(Double(sum)))
    }
}
struct Guess {
    var correctAnswer:Int
    var numAttemptsRequired:Int
}
enum Result:String {case tooLow = "Too Low", tooHigh = "Too High", correct = "Correct"}

